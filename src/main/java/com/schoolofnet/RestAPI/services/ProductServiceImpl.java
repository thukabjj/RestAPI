package com.schoolofnet.RestAPI.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.schoolofnet.RestAPI.models.Product;
import com.schoolofnet.RestAPI.repository.ProductRepository;

@Service
public class ProductServiceImpl implements ProductService {
	
	@Autowired
	private ProductRepository productRepository;
	
	public ProductServiceImpl(ProductRepository productRepository) {
		this.productRepository = productRepository;
	}
	
	@Override
	public List<Product> findAll() {
		
		return this.productRepository.findAll();
	}

	@Override
	public Product find(Long id) {
		Optional<Product> pro = this.productRepository.findById(id);
		Product produto = pro.get();
		return produto;
	}

	@Override
	public Product create(Product product) {
		this.productRepository.save(product);
		return product;
	}

	@Override
	public Product update(Long id, Product product) {
		Optional<Product> productExists = this.productRepository.findById(id);
		
		
		Product pro = productExists.get();
		
		if(pro != null ) {
			product.setId(pro.getId());
			this.productRepository.save(product);
			return product;
		}
		return null;
	}

	@Override
	public void delete(Long id) {
		Optional<Product> product = this.productRepository.findById(id);
		if(product != null) this.productRepository.deleteById(id);
	}

}
