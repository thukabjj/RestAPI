package com.schoolofnet.RestAPI.models;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.validator.constraints.Length;

@Entity
public class Product {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotEmpty(message = "Cannot be Empty")
	@NotBlank(message = "Cannot be Empty")
	@Length(min = 4,max = 255)
	private String name;
	
	

	@Min(value = 0)
	@Max(value = 1000)
	private Integer qtd;
	
	@CreationTimestamp
	private Date dateCrated;
	
	public Product() {}
	
	
	public Product(String name, Integer qtd) {
		this.name = name;
		this.qtd = qtd;
	}
	
	@PrePersist
	public void onCreate() {
		if(this.dateCrated == null) {
			//this.dateCrated = new Date(id);
		}
	}
	
	@Override
	public String toString() {
		return "{id: "+ this.id +", name: "+ this.name +", qtd: "+ this.qtd +", dateCreated: "+this.dateCrated+"}";	
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getQtd() {
		return qtd;
	}

	public void setQtd(Integer qtd) {
		this.qtd = qtd;
	}
	public Date getDateCrated() {
		return dateCrated;
	}
	public void setDateCrated(Date dateCrated) {
		this.dateCrated = dateCrated;
	}
}
